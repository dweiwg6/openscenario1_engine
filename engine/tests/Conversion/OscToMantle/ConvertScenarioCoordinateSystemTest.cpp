/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"

TEST(CoordinateSystemTest, GivenUnkownCoorodinates)
{
  auto coordinateSystem = OPENSCENARIO::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::UNKNOWN);

  ASSERT_EQ(OPENSCENARIO::CoordinateSystem::kUnknown, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenEntityCoorodinates)
{
  auto coordinateSystem = OPENSCENARIO::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::ENTITY);

  ASSERT_EQ(OPENSCENARIO::CoordinateSystem::kEntity, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenLaneCoorodinates)
{
  auto coordinateSystem = OPENSCENARIO::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::LANE);

  ASSERT_EQ(OPENSCENARIO::CoordinateSystem::kLane, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenRoadCoorodinates)
{
  auto coordinateSystem = OPENSCENARIO::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::ROAD);

  ASSERT_EQ(OPENSCENARIO::CoordinateSystem::kRoad, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenTrajectoryCoorodinates)
{
  auto coordinateSystem = OPENSCENARIO::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_1::CoordinateSystem::CoordinateSystemEnum::TRAJECTORY);

  ASSERT_EQ(OPENSCENARIO::CoordinateSystem::kTrajectory, coordinateSystem);
}