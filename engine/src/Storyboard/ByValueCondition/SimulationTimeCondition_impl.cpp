/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 in-tech GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByValueCondition/SimulationTimeCondition_impl.h"

#include <MantleAPI/Common/time_utils.h>

namespace OpenScenarioEngine::v1_1
{
bool SimulationTimeCondition::IsSatisfied() const
{
  return values.rule.IsSatisfied(mantle.environment->GetSimulationTime().convert<units::time::second>()());
}

}  // namespace OpenScenarioEngine::v1_1
