/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Conditions/Trigger.h"
#include "TestUtils.h"
#include "builders/storyboard_builder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class TriggerTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(TriggerTestFixture, GivenTriggerWithoutConditionGroups_WhenCallingIsSatisfied_ThenReturnFalse)
{
    auto fake_trigger = OPENSCENARIO::TESTING::FakeTriggerBuilder{}.Build();

    OPENSCENARIO::Trigger trigger(fake_trigger, env_);

    EXPECT_FALSE(trigger.IsSatisfied());
}

TEST_F(TriggerTestFixture, GivenTriggerWithTwoConditionGroups_WhenNoneIsSatisfied_ThenIsSatisfiedReturnsFalse)
{
    using namespace OPENSCENARIO::TESTING;
    auto by_value_condition_1 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 10.0})
                                    .Build();
    auto by_value_condition_2 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 20.0})
                                    .Build();
    auto fake_trigger =
        FakeTriggerBuilder()
            .WithConditionGroup(
                FakeConditionGroupBuilder()
                    .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_1).Build())
                    .Build())
            .WithConditionGroup(
                FakeConditionGroupBuilder()
                    .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_2).Build())
                    .Build())
            .Build();

    OPENSCENARIO::Trigger trigger(fake_trigger, env_);

    EXPECT_FALSE(trigger.IsSatisfied());
}

TEST_F(TriggerTestFixture, GivenTriggerWithTwoConditionGroups_WhenOneIsSatisfied_ThenIsSatisfiedReturnsTrue)
{
    using namespace OPENSCENARIO::TESTING;
    auto by_value_condition_1 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 20.0})
                                    .Build();
    auto by_value_condition_2 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0})
                                    .Build();
    auto fake_trigger =
        FakeTriggerBuilder()
            .WithConditionGroup(
                FakeConditionGroupBuilder()
                    .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_1).Build())
                    .Build())
            .WithConditionGroup(
                FakeConditionGroupBuilder()
                    .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_2).Build())
                    .Build())
            .Build();

    OPENSCENARIO::Trigger trigger(fake_trigger, env_);

    EXPECT_TRUE(trigger.IsSatisfied());
}

}  // namespace OPENSCENARIO
