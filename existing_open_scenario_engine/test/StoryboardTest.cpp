/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/SpeedAction.h"
#include "Storyboard/Actions/TeleportAction.h"
#include "Storyboard/Storyboard.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"
#include "builders/storyboard_builder.h"

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_1/impl/ApiClassImplV1_1.h>
#include <openScenarioLib/src/impl/NamedReferenceProxy.h>

namespace OPENSCENARIO
{

class StoryboardSUT : public Storyboard
{
  public:
    StoryboardSUT(NET_ASAM_OPENSCENARIO::v1_1::IStoryboard& storyboard,
                  std::shared_ptr<mantle_api::IEnvironment> environment)
        : Storyboard(storyboard, environment)
    {
    }

    const std::vector<std::shared_ptr<Action>>& GetInitActions() { return init_actions; }
    const std::vector<std::shared_ptr<Event>>& GetEvents() { return events; }
};

class StoryboardTestFixture : public EngineSubModuleTestBase
{
  protected:
    void LoadScenario(const std::string& scenario_file_path) override
    {
        EngineSubModuleTestBase::LoadScenario(scenario_file_path);
        storyboard_ptr_ = scenario_ptr_->GetOpenScenarioCategory()->GetScenarioDefinition()->GetStoryboard();
    }

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IStoryboard> storyboard_ptr_;
};

// Constructor
TEST_F(
    StoryboardTestFixture,
    GivenParsedStoryboardWithInitActions_WhenCreateStoryboardImplementation_ThenInitActionImplementationsAreInstantiated)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_teleport_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::TeleportActionImpl>();
    auto fake_private_action1 = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PrivateActionImpl>();
    fake_private_action1->SetTeleportAction(fake_teleport_action);
    std::string shape_name("step");
    auto fake_speed_action =
        FakeSpeedActionBuilder{}
            .WithSpeedActionTarget(FakeSpeedActionTargetBuilder{}
                                       .WithAbsoluteTargetSpeed(FakeAbsoluteTargetSpeedBuilder(0.0).Build())
                                       .Build())
            .WithTransitionDynamics(FakeTransitionDynamicsBuilder{}.WithDynamicsShape(shape_name).Build())
            .Build();
    auto fake_longitudinal_action = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::LongitudinalActionImpl>();
    fake_longitudinal_action->SetSpeedAction(fake_speed_action);
    auto fake_private_action2 = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PrivateActionImpl>();
    fake_private_action2->SetLongitudinalAction(fake_longitudinal_action);
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPrivateActionWriter>> fake_private_actions{};
    fake_private_actions.push_back(fake_private_action1);
    fake_private_actions.push_back(fake_private_action2);
    auto fake_named_reference_proxy =
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>("Ego");
    auto fake_private = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::PrivateImpl>();
    fake_private->SetPrivateActions(fake_private_actions);
    fake_private->SetEntityRef(fake_named_reference_proxy);
    std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IPrivateWriter>> fake_privates{};
    fake_privates.push_back(fake_private);
    auto fake_init_actions = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::InitActionsImpl>();
    fake_init_actions->SetPrivates(fake_privates);
    auto fake_init = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::InitImpl>();
    fake_init->SetActions(fake_init_actions);
    auto fake_storyboard = std::make_shared<NET_ASAM_OPENSCENARIO::v1_1::StoryboardImpl>();
    fake_storyboard->SetInit(fake_init);

    StoryboardSUT storyboard{*fake_storyboard, env_};
    auto init_actions = storyboard.GetInitActions();
    ASSERT_EQ(2, init_actions.size());

    ASSERT_TRUE(std::dynamic_pointer_cast<TeleportAction>(init_actions[0]));
    ASSERT_TRUE(std::dynamic_pointer_cast<SpeedAction>(init_actions[1]));

    auto teleport_action = std::dynamic_pointer_cast<TeleportAction>(init_actions[0]);
    ASSERT_EQ(1, teleport_action->GetActors().size());
    EXPECT_EQ("Ego", teleport_action->GetActors()[0]);
    EXPECT_TRUE(teleport_action->GetTeleportActionData());
}

TEST_F(StoryboardTestFixture,
       GivenParsedStoryboardWithEvents_WhenCreateStoryboardImplementation_ThenEventImplementationsAreInstantiated)
{
    using namespace OPENSCENARIO::TESTING;
    using namespace NET_ASAM_OPENSCENARIO::v1_1;
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0})
                                  .Build();
    auto fake_event =
        FakeEventBuilder()
            .WithStartTrigger(
                FakeTriggerBuilder()
                    .WithConditionGroup(
                        FakeConditionGroupBuilder()
                            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition).Build())
                            .Build())
                    .Build())
            .WithAction(FakeActionBuilder()
                            .WithPrivateAction(
                                FakePrivateActionBuilder()
                                    .WithTeleportAction(
                                        FakeTeleportActionBuilder()
                                            .WithPosition(FakePositionBuilder()
                                                              .WithLanePosition(
                                                                  FakeLanePositionBuilder("1", "2", 3.0, 4.0).Build())
                                                              .Build())
                                            .Build())
                                    .Build())
                            .Build())
            .Build();
    std::vector<std::shared_ptr<IEventWriter>> fake_events;
    fake_events.push_back(fake_event);
    fake_events.push_back(fake_event);
    auto fake_maneuver = std::make_shared<ManeuverImpl>();
    fake_maneuver->SetEvents(fake_events);
    std::vector<std::shared_ptr<IManeuverWriter>> fake_maneuvers;
    fake_maneuvers.push_back(fake_maneuver);
    auto fake_maneuver_group = std::make_shared<ManeuverGroupImpl>();
    fake_maneuver_group->SetManeuvers(fake_maneuvers);
    std::vector<std::shared_ptr<IManeuverGroupWriter>> fake_maneuver_groups;
    fake_maneuver_groups.push_back(fake_maneuver_group);
    auto fake_act = std::make_shared<ActImpl>();
    fake_act->SetManeuverGroups(fake_maneuver_groups);
    std::vector<std::shared_ptr<IActWriter>> fake_acts;
    fake_acts.push_back(fake_act);
    auto fake_story = std::make_shared<StoryImpl>();
    fake_story->SetActs(fake_acts);
    std::vector<std::shared_ptr<IStoryWriter>> fake_stories;
    fake_stories.push_back(fake_story);
    auto fake_storyboard = std::make_shared<StoryboardImpl>();
    fake_storyboard->SetStories(fake_stories);

    StoryboardSUT storyboard{*fake_storyboard, env_};
    EXPECT_EQ(2, storyboard.GetEvents().size());
}

// Init
TEST_F(StoryboardTestFixture, GivenStoryboard_WhenInitCalled_ThenNoThrow)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    LoadScenario(xosc_file_path);
    Storyboard storyboard{*storyboard_ptr_, env_};

    EXPECT_NO_THROW(storyboard.Init());
}

// Step
TEST_F(StoryboardTestFixture, GivenParsedStoryboardWithEvent_WhenStepStoryboard_ThenActionIsStepped)
{
    using namespace OPENSCENARIO::TESTING;
    using namespace NET_ASAM_OPENSCENARIO::v1_1;
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0})
                                  .Build();
    auto fake_event =
        FakeEventBuilder()
            .WithStartTrigger(
                FakeTriggerBuilder()
                    .WithConditionGroup(
                        FakeConditionGroupBuilder()
                            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition).Build())
                            .Build())
                    .Build())
            .WithAction(FakeActionBuilder()
                            .WithPrivateAction(
                                FakePrivateActionBuilder()
                                    .WithTeleportAction(
                                        FakeTeleportActionBuilder()
                                            .WithPosition(FakePositionBuilder()
                                                              .WithLanePosition(
                                                                  FakeLanePositionBuilder("1", "2", 3.0, 4.0).Build())
                                                              .Build())
                                            .Build())
                                    .Build())
                            .Build())
            .Build();
    std::vector<std::shared_ptr<IEventWriter>> fake_events;
    fake_events.push_back(fake_event);
    auto fake_maneuver = std::make_shared<ManeuverImpl>();
    fake_maneuver->SetEvents(fake_events);
    std::vector<std::shared_ptr<IManeuverWriter>> fake_maneuvers;
    fake_maneuvers.push_back(fake_maneuver);
    auto fake_maneuver_group = std::make_shared<ManeuverGroupImpl>();
    fake_maneuver_group->SetManeuvers(fake_maneuvers);
    auto fake_actors = std::make_shared<ActorsImpl>();
    auto fake_entity_ref = std::make_shared<EntityRefImpl>();
    auto fake_named_ref =
        std::make_shared<NET_ASAM_OPENSCENARIO::NamedReferenceProxy<NET_ASAM_OPENSCENARIO::v1_1::IEntity>>();
    fake_entity_ref->SetEntityRef(fake_named_ref);
    std::vector<std::shared_ptr<IEntityRefWriter>> fake_entity_refs;
    fake_entity_refs.push_back(fake_entity_ref);
    fake_actors->SetEntityRefs(fake_entity_refs);
    fake_maneuver_group->SetActors(fake_actors);
    std::vector<std::shared_ptr<IManeuverGroupWriter>> fake_maneuver_groups;
    fake_maneuver_groups.push_back(fake_maneuver_group);
    auto fake_act = std::make_shared<ActImpl>();
    fake_act->SetManeuverGroups(fake_maneuver_groups);
    std::vector<std::shared_ptr<IActWriter>> fake_acts;
    fake_acts.push_back(fake_act);
    auto fake_story = std::make_shared<StoryImpl>();
    fake_story->SetActs(fake_acts);
    std::vector<std::shared_ptr<IStoryWriter>> fake_stories;
    fake_stories.push_back(fake_story);
    auto fake_storyboard = std::make_shared<StoryboardImpl>();
    fake_storyboard->SetStories(fake_stories);

    StoryboardSUT storyboard{*fake_storyboard, env_};

    EXPECT_CALL(dynamic_cast<mantle_api::MockVehicle&>(env_->GetEntityRepository().Get("").value().get()),
                SetPosition(testing::_))
        .Times(1);
    storyboard.Init();
    storyboard.Step();
}

// IsFinished
TEST_F(StoryboardTestFixture, GivenStoryboard_WhenInitIsNotCalled_ThenIsFinishedIsTrue)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    LoadScenario(xosc_file_path);
    Storyboard storyboard{*storyboard_ptr_, env_};

    EXPECT_TRUE(storyboard.IsFinished());
}

TEST_F(StoryboardTestFixture, GivenStoryboard_WhenInitIsCalled_ThenIsFinishedIsFalse)
{
    std::string xosc_file_path{GetScenariosPath(test_info_->file()) + default_xosc_scenario_};
    LoadScenario(xosc_file_path);
    Storyboard storyboard{*storyboard_ptr_, env_};
    storyboard.Init();
    EXPECT_FALSE(storyboard.IsFinished());
}

TEST_F(StoryboardTestFixture,
       GivenScenarioWithSimulationTimeStopTrigger_WhenStepAfterSimulationTime_ThenIsFinishedIsTrue)
{
    using namespace OPENSCENARIO::TESTING;
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 10.0})
                                  .Build();
    auto stop_trigger = FakeTriggerBuilder().WithConditionGroup(
        FakeConditionGroupBuilder()
            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition).Build())
            .Build());

    FakeStoryboardBuilder storyboard_builder{};
    storyboard_builder.WithStopTrigger(stop_trigger.Build());

    auto parsed_storyboard = storyboard_builder.Build();
    OPENSCENARIO::Storyboard storyboard{parsed_storyboard, env_};

    EXPECT_CALL(*env_, GetSimulationTime)
        .WillOnce(testing::Return(units::time::second_t(9.0)))
        .WillRepeatedly(testing::Return(units::time::second_t(11.0)));

    storyboard.Init();
    storyboard.Step();
    EXPECT_FALSE(storyboard.IsFinished());

    storyboard.Step();
    EXPECT_TRUE(storyboard.IsFinished());

    storyboard.Step();
    EXPECT_TRUE(storyboard.IsFinished());
}

// GetDuration
TEST_F(StoryboardTestFixture, GivenScenarioWithOneSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsDuration)
{
    using namespace OPENSCENARIO::TESTING;
    double expected_result = 10.0;
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, expected_result})
                                  .Build();
    auto stop_trigger = FakeTriggerBuilder().WithConditionGroup(
        FakeConditionGroupBuilder()
            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition).Build())
            .Build());

    FakeStoryboardBuilder storyboard_builder{};
    storyboard_builder.WithStopTrigger(stop_trigger.Build());

    auto parsed_storyboard = storyboard_builder.Build();
    OPENSCENARIO::Storyboard storyboard{parsed_storyboard, env_};

    EXPECT_EQ(units::time::second_t(expected_result), storyboard.GetDuration());
}

TEST_F(StoryboardTestFixture, GivenScenarioWithTwoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDuration)
{
    using namespace OPENSCENARIO::TESTING;
    double expected_result = 20.0;
    auto by_value_condition_1 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, 10.0})
                                    .Build();
    auto by_value_condition_2 = FakeByValueConditionBuilder{}
                                    .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                        NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::GREATER_THAN, expected_result})
                                    .Build();
    auto stop_trigger = FakeTriggerBuilder().WithConditionGroup(
        FakeConditionGroupBuilder()
            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_1).Build())
            .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition_2).Build())
            .Build());

    FakeStoryboardBuilder storyboard_builder{};
    storyboard_builder.WithStopTrigger(stop_trigger.Build());

    auto parsed_storyboard = storyboard_builder.Build();
    OPENSCENARIO::Storyboard storyboard{parsed_storyboard, env_};

    EXPECT_EQ(units::time::second_t(expected_result), storyboard.GetDuration());
}

TEST_F(StoryboardTestFixture, GivenScenarioWithNoSimulationTimeStopTrigger_WhenGetDuration_ThenReturnsMaxDouble)
{
    using namespace OPENSCENARIO::TESTING;
    auto parsed_storyboard = FakeStoryboardBuilder().WithStopTrigger(FakeTriggerBuilder().Build()).Build();

    OPENSCENARIO::Storyboard storyboard{parsed_storyboard, env_};

    EXPECT_EQ(units::time::second_t(std::numeric_limits<double>::max()), storyboard.GetDuration());
}

}  // namespace OPENSCENARIO
