/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Actions/CustomCommandAction.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace units::literals;

class CustomCommandActionTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

TEST_F(CustomCommandActionTestFixture, GivenCustomCommandAction_WhenStart_ThenForwardsGivenCommand)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_custom_command_action = FakeCustomCommandActionBuilder{"MyType", "MyCommand"}.Build();

    OPENSCENARIO::CustomCommandAction custom_command_action{
        fake_custom_command_action, env_, std::vector<std::string>{"Ego"}};

    EXPECT_CALL(*env_, ExecuteCustomCommand({{"Ego"}}, "MyType", "MyCommand")).Times(1);

    EXPECT_NO_THROW(custom_command_action.Start());
}

TEST_F(CustomCommandActionTestFixture, GivenCustomCommandAction_WhenStepping_ThenCompletes)
{
    using namespace OPENSCENARIO::TESTING;
    auto fake_custom_command_action = FakeCustomCommandActionBuilder{"MyType", "MyCommand"}.Build();

    OPENSCENARIO::CustomCommandAction custom_command_action{
        fake_custom_command_action, env_, std::vector<std::string>{"Ego"}};

    custom_command_action.Start();
    custom_command_action.Step();
    EXPECT_TRUE(custom_command_action.isComplete());
}

}  // namespace OPENSCENARIO
