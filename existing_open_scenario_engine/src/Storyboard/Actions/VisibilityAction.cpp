/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "VisibilityAction.h"

namespace OPENSCENARIO
{

VisibilityAction::VisibilityAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IVisibilityAction> visibility_action_data,
                               std::shared_ptr<mantle_api::IEnvironment> environment,
                               const std::vector<std::string>& actors)
    : PrivateAction(environment, actors), visibility_action_data_(visibility_action_data)
{
}

void VisibilityAction::Step()
{
    if(isRunning())
    {
        End();
    }
}

void VisibilityAction::ExecuteStartTransition()
{
    for (const auto& actor : actors_)
    {
        auto & entity = environment_->GetEntityRepository().Get(actor).value().get();
        mantle_api::EntityVisibilityConfig vis_config{visibility_action_data_->GetGraphics(), visibility_action_data_->GetTraffic(), visibility_action_data_->GetSensors()};
        entity.SetVisibility(vis_config);
    }
}

}  // namespace OPENSCENARIO
