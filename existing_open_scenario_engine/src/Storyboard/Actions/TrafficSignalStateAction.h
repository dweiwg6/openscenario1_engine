/*******************************************************************************
 * Copyright (c) 2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "GlobalAction.h"

#include <vector>
#include <string>
#include <memory>

namespace OPENSCENARIO
{

class TrafficSignalStateAction : public GlobalAction
{
  public:
    TrafficSignalStateAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateAction> traffic_signal_state_action_data,
        std::shared_ptr<mantle_api::IEnvironment> environment);

    void ExecuteStartTransition() override;
    void Step() override;

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ITrafficSignalStateAction> traffic_signal_state_action_data_;
};

}  // namespace OPENSCENARIO
